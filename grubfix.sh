#!/bin/bash
echo "Power Progress Community - www.powerprogress.org"
echo "------------------------------------------------"
echo "Debian Grub fix script"

if [ "$#" -eq 1 ]
then
	echo "Download and install HFS progs"
	wget https://repo.powerprogress.org/debian/install/hfsprogs_332.25-11+b3_ppc64.deb
	dpkg -i hfsprogs_332.25-11+b3_ppc64.deb

	echo "Creating hfs file system over" $1
	mkfs.hfs -h $1
	mkdir -p "/boot/grub" && mount -t hfs $1 "/boot/grub"
	echo "$1  /boot/grub hfs defaults 0 2" >> /etc/fstab
	
	echo "Installing GRUB over" $1
	mv /usr/sbin/ofpathname /usr/sbin/ofpathname-ibm && ln -s /usr/sbin/ofpath /usr/sbin/ofpathname
	grub-install  --macppc-directory=/boot/grub
	grub-mkconfig -o /boot/grub/grub.cfg

	echo "Setting up OpenFirmware env parameter"
	nvsetenv boot-device "$( ofpath  $1 ),\grub"
	nvram --print-config=boot-device
	
	echo "Grub is now installed on your $1 partition"
	
	echo "Add Windfarm to /etc/modules  (y/n)?"
	read azione
	if [ "$azione" == 'y' ]
	then
		echo "windfarm_core" >> /etc/modules
	fi

	echo "Add Power Progress Community Repository to /etc/sources.list  (y/n) ?"
	read azione
	if [ "$azione" == 'y' ]
	then
		sh -c "echo 'deb http://repo.powerprogress.org/debian sid main' >> /etc/apt/sources.list"
		wget -O - https://repo.powerprogress.org/debian/conf/public.gpg.key|sudo -s apt-key add
		apt-get update 

		sh -c "echo 'deb http://ftp.debian.org/debian/ sid contrib non-free' >> /etc/apt/sources.list"
		apt-get -y install firmware-b43-installer
		apt-get -y install firmware-amd-graphics 
	fi

	echo "Install LXDE and other software (y/n) ?"
	read azione
	if [ "$azione" == 'y' ]
	then
		apt-get -y install task-lxde-desktop
		apt-get -y install arcticfox
		apt-get -y install powermoon
		apt-get -y install fs-uae
		apt-get -y install mame
		apt-get -y install vlc 		
	fi
	
	echo "Finished, type exit and reboot your system"
	
else
	echo "enter your boot partition name i.e. /dev/sdb2"
fi
